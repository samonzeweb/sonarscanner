#ifndef SAMONZEWEB_SONARSCANNER_SCANNER_H
#define SAMONZEWEB_SONARSCANNER_SCANNER_H

namespace Samonzeweb {
  namespace SonarScanner {

    class Scanner {
      public:
        Scanner(byte initServoSonarPin, int initServoTrim, int initServoSpeed,
                byte initTriggerPin, byte initEchoPin);
        Scanner(byte initServoSonarPin, int initServoTrim,
                byte initTriggerPin, byte initEchoPin);

        float    SingleMeasure(byte angle);
        Results *Scan(byte minAngle, byte maxAngle, byte angleIncrement);

        // Servo trim limits
        static const int minServoTrim = -20;
        static const int maxServoTrim =  20;

        // Default servo speed if none if given, in ms/60° .
        // The default value is high to avoid bad measures with slow servos.
        static const int defaultServoSpeed = 330;
        // Servo speed limits (very large range)
        static const int minServoSpeed =   20;
        static const int maxServoSpeed = 1000;

      private:
        void Init(byte initServoSonarPin, int initServoTrim, int initServoSpeed,
                  byte initTriggerPin, byte initEchoPin);
        void MoveServo(byte angle);
        byte AngleCorrection(byte angle);
        void Wait(int deltaAngle);

        byte     servoSonarPin;
        int      servoTrim;
        Servo    servo;
        int      servoSpeed;
        Samonzeweb::HR_SR04 *Sonar;
    };

  }
}

#endif
