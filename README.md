# Sonar Scanner

The SonarScanner library control a servo and a HC_SR04 sensor monted on it.

It is useful for wide scan of environment. With the library you juste give order and get all results. It can also take single measure if needed.

The SonarScanner library require anoter external library for HC_SR04 available here : https://bitbucket.org/samonzeweb/hc_sr04

# Speed and timing

The HC_SR04 library take care to have at least 50ms between measures as required by hardware specifications.

The SonarScanner take care of servo speed to wait until it's in position before start the measure. It use the servo speed information. You can find yours here : http://www.servodatabase.com/ . Don't use the spec value, use a bigger one. How many ? Guesswork...

The usual unit is s/60° (how long it take to rotate 60°). The SonarScanner take ms/60° to avoid useless float type, just multiply the value by 1000.

If you don't know (ou don't bother) use the constructor without speed. It use a reasonably high default value.

# Constructor

**DON'T** use the constructor in global variables declaration ! The instance have to be used in `setup()` or later. Act like the example below.

# Example

```
// Arduino libraries
#include <Servo.h>

// https://bitbucket.org/samonzeweb/hc_sr04
#include <HC_SR04.h>

// https://bitbucket.org/samonzeweb/sonarscanner
#include <SonarScanner.h>

using namespace Samonzeweb;

// Sonar HC_SR04
const byte triggerPin = 13;
const byte echoPin = 12;

// Sonar direction, adjust angle to have real 90°
const byte servoSonarPin = 11;
const int  adjustCenter  = 5;
// Servo speed. Mine is 120ms/60°. I use 200 to have a big margin
const int servoSpeed = 200;
// SonarScanner
SonarScanner::Scanner *sonarScanner;

void setup() {
  Serial.begin(9600);
  Serial.println("Ready");
  sonarScanner = new SonarScanner::Scanner(servoSonarPin, adjustCenter, servoSpeed, triggerPin, echoPin);
  // Whithout servo speed information (use default value)
  // sonarScanner = new SonarScanner::Scanner(servoSonarPin, adjustCenter, triggerPin, echoPin);
  Serial.println("Center");
  delay(5000);
}

void loop() {
  for(byte i=45;i<=135;i+=10) {
    Serial.println("Single mesure :");
    Serial.println(sonarScanner->SingleMeasure(i));
    Serial.println();
    delay(500);
  }

  Serial.println("Scan...");
  SonarScanner::Results *results = sonarScanner->Scan(10, 170, 10);
  for(int a=0; a<results->MesuresCount(); a++)
  Serial.println(results->Read(a));
  Serial.println();
  delete results;
  delay(5000);
}
```
