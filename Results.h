#ifndef SAMONZEWEB_SONARSCANNER_RESULTS_H
#define SAMONZEWEB_SONARSCANNER_RESULTS_H

namespace Samonzeweb {
  namespace SonarScanner {

    class Results {
      public:
        Results(byte initMinAngle, byte initMaxAgnel, byte initAngleIncrement);
        ~Results();
        void Write(byte count, float distance);
        float Read(byte count);

        byte   MinAngle();
        byte   MaxAngle();
        byte   AngleIncrement();
        byte   MesuresCount();

      private:
        byte   minAngle;
        byte   maxAngle;
        byte   angleIncrement;
        byte   mesuresCount;
        float *distancesCm;
    };

  }
}

#endif
