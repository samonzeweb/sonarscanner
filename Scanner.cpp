#include <Arduino.h>
#include <Servo.h>

#include "../HC_SR04/HC_SR04.h"
#include "Results.h"
#include "Scanner.h"

namespace Samonzeweb {
  namespace SonarScanner {

    Scanner::Scanner(byte initServoSonarPin, int initServoTrim,
                     int  initServoSpeed,
                     byte initTriggerPin, byte initEchoPin) {
      Init(initServoSonarPin, initServoTrim, initServoSpeed,
           initTriggerPin, initEchoPin);
    }

    // Light constructor : use default servo speed.
    Scanner::Scanner(byte initServoSonarPin, int initServoTrim,
                     byte initTriggerPin, byte initEchoPin) {
      Init(initServoSonarPin, initServoTrim, defaultServoSpeed,
              initTriggerPin, initEchoPin);
    }

    // Common part of constructors
    void Scanner::Init(byte initServoSonarPin, int initServoTrim,
                  int  initServoSpeed,
                  byte initTriggerPin, byte initEchoPin) {
      // Servo parameters : pin and trim (centering)
      servoSonarPin = initServoSonarPin;
      servoTrim = initServoTrim;
      if(servoTrim<minServoTrim || servoTrim>maxServoTrim)
        servoTrim = 0;
      servoSpeed = initServoSpeed;
      if(servoSpeed<minServoSpeed || servoSpeed>maxServoSpeed)
        servoSpeed = defaultServoSpeed;
      // Servo initialization
      servo.attach(servoSonarPin);
      MoveServo(90);
      // Sonar setup
      Sonar = new Samonzeweb::HR_SR04(initTriggerPin, initEchoPin);
    }

    // Take a single mesure
    float Scanner::SingleMeasure(byte angle) {
      if(angle<0 || angle>180)
        return -1;
      MoveServo(angle);
      return Sonar->DistanceCm();
    }

    // Return a group of measures
    // The Results instance have to be deleted after usage to free memory !
    Results *Scanner::Scan(byte minAngle, byte maxAngle, byte angleIncrement) {
      Results *results;
      byte     angle;

      results = new Results(minAngle, maxAngle, angleIncrement);
      angle   = results->MinAngle();
      for(byte count = 0; count < results->MesuresCount(); count++) {
        MoveServo(angle);
        results->Write(count, Sonar->DistanceCm());
        angle += results->AngleIncrement();
      }
      MoveServo(90);
      return results;
    }

    // Move Servo. What else ?
    void Scanner::MoveServo(byte angle) {
      byte targetAngle = AngleCorrection(angle);
      int deltaAngle = servo.read() - targetAngle;
      servo.write(targetAngle);
      Wait(deltaAngle);
    }

    // Manage servo trim
    byte Scanner::AngleCorrection(byte angle) {
      int correctedAngle = angle + servoTrim;
      if(correctedAngle < 0)
      {
        correctedAngle = 0;
      } else {
        if (correctedAngle > 180)
          correctedAngle = 180;
      }
      return correctedAngle;
    }

    // Wait to allow the servo reach the goog angle
    void Scanner::Wait(int deltaAngle) {
      // long cast is required because deltaAngle * servoSpeed does not
      // always fit in an int
      int timeToWait = (long)abs(deltaAngle) * servoSpeed / 60;
      delay(timeToWait);
    }
  }
}
