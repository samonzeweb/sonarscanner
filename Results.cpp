#include <Arduino.h>

#include "Results.h"

namespace Samonzeweb {
  namespace SonarScanner {

    Results::Results(byte initMinAngle, byte initMaxAngle, byte initAngleIncrement) {
      // Always within [0° , 180°] to avoir servo destruction
      minAngle = initMinAngle;
      if(minAngle<0)
        minAngle = 0;
      maxAngle = initMaxAngle;
      if(maxAngle>180)
        maxAngle = 180;
      // max >= min
      if(maxAngle < minAngle) {
        byte swap = maxAngle;
        maxAngle = minAngle;
        minAngle = swap;
      }
      // increment is greater then 0
      angleIncrement = initAngleIncrement;
      if(angleIncrement<=0) {
        angleIncrement = 1;
      }
      // How many mesures, and compute real max angle
      mesuresCount   = 1 + (maxAngle - minAngle) / angleIncrement;
      maxAngle       = minAngle + angleIncrement * mesuresCount;
      // Memory for mesures
      distancesCm    = new float[mesuresCount];
    }

    Results::~Results() {
      delete distancesCm;
    }

    void Results::Write(byte count, float distance) {
      if(count>=0 && count<mesuresCount)
        distancesCm[count] = distance;
    }

    float Results::Read(byte count) {
      if(count>=0 && count<mesuresCount)
        return distancesCm[count];
        else return -1;
    }

    byte Results::MinAngle() {
      return minAngle;
    }

    byte Results::MaxAngle() {
      return maxAngle;

    }

    byte Results::AngleIncrement() {
      return angleIncrement;
    }

    byte Results::MesuresCount() {
      return mesuresCount;
    }
  }
}
